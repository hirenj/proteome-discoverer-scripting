; Scripts for Automating Proteome Discoverer
; Author: Catharina Steentoft
; Copenhagen Center for Glycomics
; For user Catharina


^!p::
        IfWinExist Excel
                WinActivate
        clipboard =
        Send {ESC}
        Send ^c
        ClipWait, 1
        ;MsgBox %clipboard%
        IfWinExist Thermo Proteome Discoverer
                WinActivate
                MouseMove 247, 160
                Click 2
                Send ^v
                MouseMove 524, 183
                Click 1
        return

; use ctrl-Alt-m to decrease tolerance when given in Da
^!m::
        Send !{F1}
        WinWaitActive, Peptide Consensus View, , 2
        WinMaximize
        Click 419, 845
        Send A
        Sleep 1000
        MouseMove 254, 848
        Click 2
        Send {BS}
        Send 1
        Send {Enter}         
        return

; use ctrl-Alt-k to decrease tolerance when given in mmu
^!k::
        Send !{F1}
        WinWaitActive, Peptide Consensus View, , 2
        WinMaximize
        Click 419, 845
        Send A
        Sleep 1000
        MouseMove 254, 848
        Click 2
        Send {BS 2}
        Send 10
        Send {Enter}
        return

^!y::
        Send !{F4}
        Send +{Right 20}
        KeyWait Shift
        clipboard =
        Send ^c
        ClipWait, 1
 
        SetTitleMatchMode, 2
        IfWinExist Excel
                WinActivate
                Send {ESC}
                Send {TAB 4}
                Send ^v
                Send {Left 1}
                Send y
                Send {Left 3}
                Send {Down 1}
        return
 
 
^!n::
        Send !{F4}
        SetTitleMatchMode, 2
        IfWinExist Excel
                        WinActivate
                        Send {ESC}
                        Send {TAB 3}
                        Send n
                        Send {Left 3}
                        Send {Down 1}
        return
 
^!g::
        Send !{F4}
        Send +{Right 20}
        KeyWait Shift
        clipboard =
        Send ^c
        ClipWait, 1
 
        SetTitleMatchMode, 2
        IfWinExist Excel
                WinActivate
                Send {ESC}
                Send {TAB 4}
                Send ^v
                Send {Left 1}
                Send y
                Send {Left 5}
                Send Good HCD
                Send {Right 2}
                Send {Down 1}
        return
 
^!a::
        Send !{F4}
        Send +{Right 20}
        KeyWait Shift
        clipboard =
        Send ^c
        ClipWait, 1
 
        SetTitleMatchMode, 2
        IfWinExist Excel
                WinActivate
                Send {ESC}
                Send {TAB 4}
                Send ^v
                Send {Left 1}
                Send y
                Send {Left 6}
               
        return