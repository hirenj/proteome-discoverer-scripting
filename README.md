# Proteome Discoverer Scripting

## Introduction

Uses [AutoHotKey](http://http://www.autohotkey.com/) to automate some of the repetetive aspects of Spectra validation.

## Authors

Copenhagen Center For Glycomics

 - Catharina Steentoft
 - Hiren Joshi